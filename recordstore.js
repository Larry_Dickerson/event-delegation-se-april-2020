/*
 1. Click on a record in the display rack to add it to the cart.
    1. Click handler that copies the record div into the cart div
        ?? How do we make a copy of a div ??
        appendChild to add the copy into the cart
 2. Visually indicate that the record is selected.
    ?? highlight, border?
    -- Don't allow a shopper to select the same record twice
 3. Click a record in the cart to remove it from the cart.
    Click handler for records in the cart to 1) remove the div, 2) deselect
    the record in the display rack
*/

const addRecordToCart = function (evt) {
    evt.stopPropagation()

    // guard clause
    if (evt.target.className !== "record") {
        return
    }

    const recordNode = evt.target
    const recordCopy = recordNode.cloneNode(true)
    recordCopy.id += "-incart"
    const cart = document.querySelector("#cart")
    cart.appendChild(recordCopy)
    
    recordNode.classList.add("selected")
}

const removeRecordFromCart = function (recordEl) {
    const cart = document.getElementById("cart")
    cart.removeChild(recordEl)
}

const deselectRecord = function (recordId) {
    const recordInDisplay = document.getElementById(recordId.slice(0, -7))
    recordInDisplay.classList.remove("selected")
}

const cartClickHandler = function (evt) {
    const recordInCart = evt.target
    deselectRecord(recordInCart.id)
    removeRecordFromCart(recordInCart)
}

const rack = document.querySelector('#main')
rack.addEventListener('click', addRecordToCart)

const cart = document.querySelector('#cart')
cart.addEventListener('click', cartClickHandler)
